package eager
import com.google.inject.AbstractModule

// A Module is needed to register bindings

class EagerLoaderModule extends AbstractModule {

    override def configure(): Unit = {
        bind(classOf[StartUpService]).asEagerSingleton()
    }

}
