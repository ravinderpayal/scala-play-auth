name := "rapidfork-auth"
organization := "com.rapidfork"

version := "1.0"

lazy val auth = (project in file("auth")).enablePlugins(PlayScala)

lazy val root = (project in file(".")).enablePlugins(PlayScala)
  .dependsOn(auth).aggregate(auth)

scalaVersion := "2.13.3"

libraryDependencies ++= Seq(
  guice,
  "com.typesafe.play" %% "play-slick" % "5.0.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "5.0.0",
  "mysql" % "mysql-connector-java" % "8.0.21",
)
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test

libraryDependencies += "com.typesafe.play" %% "play-json" % "2.9.0"

