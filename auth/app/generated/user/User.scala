package com.rapidfork.auth.generated.user

import play.api.libs.json.{JsValue, Json, OFormat, Format, JsString, JsSuccess}
import slick.jdbc.MySQLProfile.api._
import java.sql.Timestamp
import java.text.SimpleDateFormat
import slick.sql.SqlProfile.ColumnOption.SqlType



object UserSchema {

  case class User(
        id: Long,
        firstName: String, 
        lastName: String, 
        mobile: Long, 
        email: Option[String], 
        password: String, 
        createdAt: Timestamp,
        updatedAt: Timestamp
  ) {
    def toPublic: UserPublic = UserPublic(id, firstName, lastName, mobile, email, createdAt, updatedAt)
  }
  object User {
    implicit object timestampFormat extends Format[Timestamp] {
      val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'")
      def reads(json: JsValue): JsSuccess[Timestamp] = {
        val str = json.as[String]
        JsSuccess(new Timestamp(format.parse(str).getTime))
      }
      def writes(ts: Timestamp): JsString = JsString(format.format(ts))
    }
    implicit val format: OFormat[User] = Json.format[User]
  }

  class UserTable(tag: Tag) extends Table[User](tag, "user") {

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc, O.Unique)

    def firstName = column[String]("first_name")

    def lastName = column[String]("last_name")

    def mobile = column[Long]("mobile")

    def email = column[Option[String]]("email")

    def password = column[String]("password")

    def createdAt = column[Timestamp]("createdAt", SqlType("timestamp not null default CURRENT_TIMESTAMP"))

    def updatedAt = column[Timestamp]("updatedAt", SqlType("timestamp not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"))

    def * = (id, firstName, lastName, mobile, email, password, createdAt, updatedAt) <> ((User.apply _).tupled, User.unapply)

    
  }

  val users = TableQuery[UserTable]

  val userSchemaCreate = users.schema.create.asTry
}


case class UserPublic(
                        id: Long,
                        firstName: String,
                        lastName: String,
                        mobile: Long,
                        email: Option[String],
//                        password: String,
                        createdAt: Timestamp,
                        updatedAt: Timestamp
                      ) {
  def toJsValue: JsValue = Json.toJson(this)
  override def toString: String = toJsValue.toString()
}

object UserPublic {
  implicit object timestampFormat extends Format[Timestamp] {
    val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'")
    def reads(json: JsValue): JsSuccess[Timestamp] = {
    val str = json.as[String]
      JsSuccess(new Timestamp(format.parse(str).getTime))
    }
    def writes(ts: Timestamp): JsString = JsString(format.format(ts))
  }
  implicit val format: OFormat[UserPublic] = Json.format[UserPublic]
}

