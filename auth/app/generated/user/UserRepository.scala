package com.rapidfork.auth.generated.user

import javax.inject._
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent.{ExecutionContext, Future}
import slick.jdbc.JdbcProfile
import slick.jdbc.MySQLProfile.api._

@Singleton
class UserRepository @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)(implicit val ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  val users = TableQuery[UserSchema.UserTable]

  def add(user: AddUserDto): Future[String] = {
    dbConfig.db.run(users.map(x => (x.firstName, x.lastName, x.mobile, x.email, x.password)) += (user.firstName, user.lastName, user.mobile, user.email.map(_.toLowerCase), user.password)).map(res => "User successfully added").recover {
      case ex: Exception => ex.getCause.getMessage
    }
  }

  def delete(id: Long): Future[Int] = {
    dbConfig.db.run(users.filter(_.id === id).delete)
  }

  def get(id: Long): Future[Option[UserSchema.User]] = {
    dbConfig.db.run(users.filter(_.id === id).result.headOption)
  }

  def listAll: Future[Seq[UserSchema.User]] = {
    dbConfig.db.run(users.result)
  }

}

