package com.rapidfork.auth.generated.user

import javax.inject._
import play.api.mvc._
import play.api.libs.json.{JsError, JsSuccess, Json}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

import com.rapidfork.auth.generated.user._

@Singleton
class UserController @Inject()(val controllerComponents: ControllerComponents, userService: UserService)(implicit val ec: ExecutionContext) extends BaseController {

  def index: Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    userService.listAllUsers.map(
      users => Ok(Json.toJson(users.map(_.toPublic)))
    )

  }

}

