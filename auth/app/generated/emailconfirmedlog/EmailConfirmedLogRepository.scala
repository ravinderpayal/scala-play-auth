package com.rapidfork.auth.generated.emailconfirmedlog

import javax.inject._
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent.{ExecutionContext, Future}
import slick.jdbc.JdbcProfile
import slick.jdbc.MySQLProfile.api._

@Singleton
class EmailConfirmedLogRepository @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)(implicit val ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  val emailconfirmedlogs = TableQuery[EmailConfirmedLogSchema.EmailConfirmedLogTable]

  def add(emailconfirmedlog: AddEmailConfirmedLogDto): Future[String] = {
    dbConfig.db.run(emailconfirmedlogs.map(x => (x.userId, x.email, x.token, x.isExpired, x.isActive)) += (emailconfirmedlog.userId, emailconfirmedlog.email, emailconfirmedlog.token, emailconfirmedlog.isExpired, emailconfirmedlog.isActive)).map(res => "EmailConfirmedLog successfully added").recover {
      case ex: Exception => ex.getCause.getMessage
    }
  }

  def delete(id: Long): Future[Int] = {
    dbConfig.db.run(emailconfirmedlogs.filter(_.id === id).delete)
  }

  def get(id: Long): Future[Option[EmailConfirmedLogSchema.EmailConfirmedLog]] = {
    dbConfig.db.run(emailconfirmedlogs.filter(_.id === id).result.headOption)
  }

  def listAll: Future[Seq[EmailConfirmedLogSchema.EmailConfirmedLog]] = {
    dbConfig.db.run(emailconfirmedlogs.result)
  }

}

