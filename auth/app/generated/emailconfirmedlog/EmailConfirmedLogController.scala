package com.rapidfork.auth.generated.emailconfirmedlog

import javax.inject._
import play.api.mvc._
import play.api.libs.json.{JsError, JsSuccess, Json}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

import com.rapidfork.auth.generated.emailconfirmedlog._

@Singleton
class EmailConfirmedLogController @Inject()(val controllerComponents: ControllerComponents, emailconfirmedlogService: EmailConfirmedLogService)(implicit val ec: ExecutionContext) extends BaseController {

  def index: Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    emailconfirmedlogService.listAllEmailConfirmedLogs.map(
      emailconfirmedlogs => Ok(Json.toJson(emailconfirmedlogs.map(_.toPublic)))
    )

  }

}

