package com.rapidfork.auth.generated.emailconfirmedlog

import play.api.libs.json.{JsValue, Json, OFormat, Format, JsString, JsSuccess}
import slick.jdbc.MySQLProfile.api._
import java.sql.Timestamp
import java.text.SimpleDateFormat
import slick.sql.SqlProfile.ColumnOption.SqlType



object EmailConfirmedLogSchema {

  case class EmailConfirmedLog(
        id: Long,
        userId: Long, 
        email: String, 
        token: String, 
        isExpired: Boolean, 
        isActive: Boolean, 
        createdAt: Timestamp,
        updatedAt: Timestamp
  ) {
    def toPublic: EmailConfirmedLogPublic = EmailConfirmedLogPublic(id, userId, email, token, isExpired, isActive, createdAt, updatedAt)
  }
  object EmailConfirmedLog {
    implicit object timestampFormat extends Format[Timestamp] {
      val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'")
      def reads(json: JsValue): JsSuccess[Timestamp] = {
        val str = json.as[String]
        JsSuccess(new Timestamp(format.parse(str).getTime))
      }
      def writes(ts: Timestamp): JsString = JsString(format.format(ts))
    }
    implicit val format: OFormat[EmailConfirmedLog] = Json.format[EmailConfirmedLog]
  }

  class EmailConfirmedLogTable(tag: Tag) extends Table[EmailConfirmedLog](tag, "emailconfirmedlog") {

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc, O.Unique)

    def userId = column[Long]("userId")

    def email = column[String]("email")

    def token = column[String]("token")

    def isExpired = column[Boolean]("isExpired", O.Default(false))

    def isActive = column[Boolean]("isActive", O.Default(false))

    def createdAt = column[Timestamp]("createdAt", SqlType("timestamp not null default CURRENT_TIMESTAMP"))

    def updatedAt = column[Timestamp]("updatedAt", SqlType("timestamp not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"))

    def * = (id, userId, email, token, isExpired, isActive, createdAt, updatedAt) <> ((EmailConfirmedLog.apply _).tupled, EmailConfirmedLog.unapply)

    
  }

  val emailconfirmedlogs = TableQuery[EmailConfirmedLogTable]

  val emailconfirmedlogSchemaCreate = emailconfirmedlogs.schema.create.asTry
}


case class EmailConfirmedLogPublic(
                        id: Long,
                        userId: Long,
                        email: String,
                        token: String,
                        isExpired: Boolean,
                        isActive: Boolean,
                        createdAt: Timestamp,
                        updatedAt: Timestamp
                      ) {
  def toJsValue: JsValue = Json.toJson(this)
  override def toString: String = toJsValue.toString()
}

object EmailConfirmedLogPublic {
  implicit object timestampFormat extends Format[Timestamp] {
    val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'")
    def reads(json: JsValue): JsSuccess[Timestamp] = {
    val str = json.as[String]
      JsSuccess(new Timestamp(format.parse(str).getTime))
    }
    def writes(ts: Timestamp): JsString = JsString(format.format(ts))
  }
  implicit val format: OFormat[EmailConfirmedLogPublic] = Json.format[EmailConfirmedLogPublic]
}

