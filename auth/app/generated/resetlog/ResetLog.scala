package com.rapidfork.auth.generated.resetlog

import play.api.libs.json.{JsValue, Json, OFormat, Format, JsString, JsSuccess}
import slick.jdbc.MySQLProfile.api._
import java.sql.Timestamp
import java.text.SimpleDateFormat
import slick.sql.SqlProfile.ColumnOption.SqlType



object ResetLogSchema {

  case class ResetLog(
        id: Long,
        userId: Long, 
        email: String, 
        token: String, 
        isExpired: Boolean, 
        isReset: Boolean, 
        createdAt: Timestamp,
        updatedAt: Timestamp
  ) {
    def toPublic: ResetLogPublic = ResetLogPublic(id, userId, email, token, isExpired, isReset, createdAt, updatedAt)
  }
  object ResetLog {
    implicit object timestampFormat extends Format[Timestamp] {
      val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'")
      def reads(json: JsValue): JsSuccess[Timestamp] = {
        val str = json.as[String]
        JsSuccess(new Timestamp(format.parse(str).getTime))
      }
      def writes(ts: Timestamp): JsString = JsString(format.format(ts))
    }
    implicit val format: OFormat[ResetLog] = Json.format[ResetLog]
  }

  class ResetLogTable(tag: Tag) extends Table[ResetLog](tag, "resetlog") {

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc, O.Unique)

    def userId = column[Long]("userId")

    def email = column[String]("email")

    def token = column[String]("token")

    def isExpired = column[Boolean]("isExpired", O.Default(false))

    def isReset = column[Boolean]("isReset", O.Default(false))

    def createdAt = column[Timestamp]("createdAt", SqlType("timestamp not null default CURRENT_TIMESTAMP"))

    def updatedAt = column[Timestamp]("updatedAt", SqlType("timestamp not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"))

    def * = (id, userId, email, token, isExpired, isReset, createdAt, updatedAt) <> ((ResetLog.apply _).tupled, ResetLog.unapply)

    
  }

  val resetlogs = TableQuery[ResetLogTable]

  val resetlogSchemaCreate = resetlogs.schema.create.asTry
}


case class ResetLogPublic(
                        id: Long,
                        userId: Long,
                        email: String,
                        token: String,
                        isExpired: Boolean,
                        isReset: Boolean,
                        createdAt: Timestamp,
                        updatedAt: Timestamp
                      ) {
  def toJsValue: JsValue = Json.toJson(this)
  override def toString: String = toJsValue.toString()
}

object ResetLogPublic {
  implicit object timestampFormat extends Format[Timestamp] {
    val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'")
    def reads(json: JsValue): JsSuccess[Timestamp] = {
    val str = json.as[String]
      JsSuccess(new Timestamp(format.parse(str).getTime))
    }
    def writes(ts: Timestamp): JsString = JsString(format.format(ts))
  }
  implicit val format: OFormat[ResetLogPublic] = Json.format[ResetLogPublic]
}

