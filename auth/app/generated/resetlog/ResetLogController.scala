package com.rapidfork.auth.generated.resetlog

import javax.inject._
import play.api.mvc._
import play.api.libs.json.{JsError, JsSuccess, Json}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

import com.rapidfork.auth.generated.resetlog._

@Singleton
class ResetLogController @Inject()(val controllerComponents: ControllerComponents, resetlogService: ResetLogService)(implicit val ec: ExecutionContext) extends BaseController {

  def index: Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    resetlogService.listAllResetLogs.map(
      resetlogs => Ok(Json.toJson(resetlogs.map(_.toPublic)))
    )

  }

}

