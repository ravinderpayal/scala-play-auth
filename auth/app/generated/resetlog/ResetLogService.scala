package com.rapidfork.auth.generated.resetlog

import javax.inject._
import scala.concurrent.Future

@Singleton
class ResetLogService @Inject() (protected val resetlogRepository: ResetLogRepository) {

  def addResetLog(resetlog: AddResetLogDto): Future[String] = {
    resetlogRepository.add(resetlog)
  }

  def deleteResetLog(id: Long): Future[Int] = {
    resetlogRepository.delete(id)
  }

  def getResetLog(id: Long): Future[Option[ResetLogSchema.ResetLog]] = {
    resetlogRepository.get(id)
  }

  def listAllResetLogs: Future[Seq[ResetLogSchema.ResetLog]] = {
    resetlogRepository.listAll
  }
}

