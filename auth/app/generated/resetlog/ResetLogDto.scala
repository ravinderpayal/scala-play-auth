package com.rapidfork.auth.generated.resetlog

import play.api.libs.json.{Json, OFormat}

case class AddResetLogDto (
                        userId:  Long,
                        email:  String,
                        token:  String,
                        isExpired:  Boolean,
                        isReset:  Boolean,
                      )

object AddResetLogDto {
  implicit val format: OFormat[AddResetLogDto] = Json.format[AddResetLogDto]
}

object ResetLogDto {

}

