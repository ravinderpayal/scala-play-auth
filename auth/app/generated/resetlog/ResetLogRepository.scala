package com.rapidfork.auth.generated.resetlog

import javax.inject._
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent.{ExecutionContext, Future}
import slick.jdbc.JdbcProfile
import slick.jdbc.MySQLProfile.api._

@Singleton
class ResetLogRepository @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)(implicit val ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  val resetlogs = TableQuery[ResetLogSchema.ResetLogTable]

  def add(resetlog: AddResetLogDto): Future[String] = {
    dbConfig.db.run(resetlogs.map(x => (x.userId, x.email, x.token, x.isExpired, x.isReset)) += (resetlog.userId, resetlog.email, resetlog.token, resetlog.isExpired, resetlog.isReset)).map(res => "ResetLog successfully added").recover {
      case ex: Exception => ex.getCause.getMessage
    }
  }

  def delete(id: Long): Future[Int] = {
    dbConfig.db.run(resetlogs.filter(_.id === id).delete)
  }

  def get(id: Long): Future[Option[ResetLogSchema.ResetLog]] = {
    dbConfig.db.run(resetlogs.filter(_.id === id).result.headOption)
  }

  def listAll: Future[Seq[ResetLogSchema.ResetLog]] = {
    dbConfig.db.run(resetlogs.result)
  }

}

