package com.rapidfork.auth.utils

import com.rapidfork.auth.application.user.UserService
import com.rapidfork.auth.generated.user.UserPublic
import javax.inject.Inject
import play.api.http.HeaderNames
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}


case class UserRequest[A](user: Option[UserPublic], token: String, request: Request[A]) extends WrappedRequest[A](request)

class AuthAction @Inject()(bodyParser: BodyParsers.Default, userService: UserService)(implicit ec: ExecutionContext)
  extends ActionBuilder[UserRequest, AnyContent] {

  override def parser: BodyParser[AnyContent] = bodyParser
  override protected def executionContext: ExecutionContext = ec

  // A regex for parsing the Authorization header value
  private val headerTokenRegex = """Bearer (.+?)""".r

  override def invokeBlock[A](request: Request[A], block: UserRequest[A] => Future[Result]): Future[Result] =
    extractBearerToken(request) map { token =>
      userService.validateJwt(token) flatMap {
        data => {
          data match {
            case Success(user) => block(UserRequest(Some(user), token, request))
            case Failure(t) => Future.successful(Results.Unauthorized(Json.toJson(Json.obj("success" -> false, "status" -> 401, "message" -> t.getMessage))))
          }
        }
      }
    } getOrElse Future.successful(Results.Unauthorized(Json.toJson(Json.obj("success" -> false, "status" -> 401, "message" -> "Auth Token is Missing"))))

  // Helper for extracting the token value
  private def extractBearerToken[A](request: Request[A]): Option[String] =
    request.headers.get(HeaderNames.AUTHORIZATION) collect {
      case headerTokenRegex(token) => token
    }
}
