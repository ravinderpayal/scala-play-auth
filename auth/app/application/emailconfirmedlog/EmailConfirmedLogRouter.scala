package com.rapidfork.auth.application.emailconfirmedlog

import javax.inject._
import play.api.routing.Router.Routes
import play.api.routing.sird._

import com.rapidfork.auth.generated.emailconfirmedlog

class EmailConfirmedLogRouter @Inject()(controller: EmailConfirmedLogController ) extends emailconfirmedlog.EmailConfirmedLogRouter(controller) {
  override def routes: Routes = super.routes orElse  {
    case GET(p"/example") => controller.example
    }
}


