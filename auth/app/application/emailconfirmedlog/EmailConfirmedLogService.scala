package com.rapidfork.auth.application.emailconfirmedlog

import javax.inject._
import scala.concurrent.{Future}
import com.rapidfork.auth.generated.emailconfirmedlog

@Singleton
class EmailConfirmedLogService @Inject()(emailconfirmedlogRepository: EmailConfirmedLogRepository) extends emailconfirmedlog.EmailConfirmedLogService(emailconfirmedlogRepository) {

}


