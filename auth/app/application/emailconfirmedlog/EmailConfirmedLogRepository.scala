package com.rapidfork.auth.application.emailconfirmedlog

import javax.inject._
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent.{ExecutionContext, Future}
import slick.jdbc.MySQLProfile.api._

import com.rapidfork.auth.generated.emailconfirmedlog

@Singleton
class EmailConfirmedLogRepository @Inject() (dbConfigProvider: DatabaseConfigProvider)(override implicit val ec: ExecutionContext) extends emailconfirmedlog.EmailConfirmedLogRepository(dbConfigProvider) {

}

