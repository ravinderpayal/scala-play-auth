package com.rapidfork.auth.application.resetlog

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}

import com.rapidfork.auth.application.user.UserService
import javax.inject._
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.{Action, AnyContent, ControllerComponents, Request, Results}

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration
import com.rapidfork.auth.generated.resetlog
import com.rapidfork.auth.generated.resetlog.AddResetLogDto
import com.rapidfork.auth.utils.{JwtUtil, SendgridEmail}
import play.api.Configuration

@Singleton
class ResetLogController @Inject()(controllerComponents: ControllerComponents, resetlogService: ResetLogService, userService: UserService, sendgridEmail: SendgridEmail)(override implicit val ec: ExecutionContext)(implicit configuration: Configuration) extends resetlog.ResetLogController(controllerComponents, resetlogService) {

  def example: Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    resetlogService.listAllResetLogs.map(
      resetlogs => Ok(Json.toJson(resetlogs.map(_.toPublic)))
    )
  }

  def forgot: Action[AnyContent] = Action.async {
    implicit request: Request[AnyContent] =>
      request.body.asJson match {
        case Some(json) =>
          (json \ "email").validate[String] match {
            case email: JsSuccess[String] =>

              userService.getUserByEmail(email.get)
                .flatMap(
                  u =>
                    if (u.isDefined) {
                      resetlogService.getResetLogByEmail(email.get.toLowerCase)
                        .flatMap(
                          rl => {
                            if (configuration.get[Boolean]("user.account.password.resetLinkExpire")) {
                              resetlogService.expireOldTokens(email.get)
                            }

                            val token = JwtUtil.generate(userId = u.get.id, expireIn = configuration.get[Long]("user.account.password.resetLinkExpireTime"))

                            resetlogService.addResetLog(AddResetLogDto(
                              userId = u.get.id,
                              email = email.get,
                              token = token,
                              isExpired = false,
                              isReset = false
                            )).flatMap(
                              nrl => {
                                val resetLink = configuration.get[String]("user.account.password.resetLinkOrigin") + s"/$token"

                                if (configuration.get[Boolean]("user.account.password.resetLinkHtmlTemplateExist")) {
                                  val fileName = configuration.get[String]("user.account.password.resetLinkHtmlTemplate")

                                  val html = new String(Files.readAllBytes(Paths.get(fileName)), StandardCharsets.UTF_8)
                                    .replace("{USER_NAME}", u.get.firstName)
                                    .replace("{RESET_LINK}", resetLink)

                                  sendgridEmail.sendMail(
                                    configuration.get[String]("user.account.password.resetLinkEmail"),
                                    "Forgot Password",
                                    email.get,
                                    s"""
                                    To reset your password open the below link in browser. If you didn't request a new password, you can safely delete this email.
                                    $resetLink

                                    Cheers
                                    """,
                                      html
                                  )
                                } else {
                                  sendgridEmail.sendMail(
                                    configuration.get[String]("user.account.password.resetLinkEmail"),
                                    "Forgot Password",
                                    email.get,
                                    s"""
                                    Hi ${u.get.firstName}
                                    To reset your password open the below link in browser. If you didn't request a new password, you can safely delete this email.
                                    $resetLink

                                    Cheers
                                    """
                                  )
                                }
                                Future { Results.Ok(Json.toJson(Json.obj("success" -> true, "status" -> 200, "message" -> "Reset Link Sent"))) }
                              }
                            )
                          }
                        )
                    } else {
                      Future { Results.NotFound(Json.toJson(Json.obj("success" -> false, "status" -> 404, "message" -> "User Not Found"))) }
                    }
                )



            case e: JsError =>
              Future { Results.BadRequest }
          }
        case None =>
          Future { Results.BadRequest }
      }
  }

  def validateToken: Action[AnyContent] = Action.async {
    implicit request: Request[AnyContent] =>
      request.body.asJson match {
        case Some(json) =>
          json.validate[VerifyResetTokenDto] match {
            case JsSuccess(requestBody, _) =>
              resetlogService.validateResetToken(requestBody.email, requestBody.token)
                .flatMap(
                  rl => {
                    if (rl.isDefined) {
                      Future { Results.Ok(Json.toJson(Json.obj("success" -> true, "status" -> 200, "message" -> "Reset Link is Valid"))) }
                    } else {
                      Future { Results.Forbidden(Json.toJson(Json.obj("success" -> false, "status" -> 401, "message" -> "Reset Link is Invalid or Expired"))) }
                    }
                  }
                )
            case e: JsError => Future {
              BadRequest(Json.toJson(Json.obj("success" -> false, "status" -> 400, "message" -> "Missing Payload")))
            }
          }
        case None => Future {
          BadRequest(Json.toJson(Json.obj("success" -> false, "status" -> 400, "message" -> "Missing Payload")))
        }
      }
  }

  def changePassword: Action[AnyContent] = Action.async {
    implicit request: Request[AnyContent] =>
      request.body.asJson match {
        case Some(json) =>
          json.validate[ChangePasswordDto] match {
            case JsSuccess(requestBody, _) =>
              resetlogService.validateResetToken(requestBody.email, requestBody.token)
                .flatMap(
                  rl => {
                    if (rl.isDefined) {
                      userService.updatePassword(requestBody.email, requestBody.password).map(up => {
                        resetlogService.updatePasswordSuccess(requestBody.email, requestBody.token)
                        resetlogService.expireOldTokens(requestBody.email)
                        up
                      })
                    } else {
                      Future { Results.Forbidden(Json.toJson(Json.obj("success" -> false, "status" -> 401, "message" -> "Reset Link is Invalid or Expired"))) }
                    }
                  }
                )
            case e: JsError => Future {
              BadRequest(Json.toJson(Json.obj("success" -> false, "status" -> 400, "message" -> "Missing Payload")))
            }
          }
        case None => Future {
          BadRequest(Json.toJson(Json.obj("success" -> false, "status" -> 400, "message" -> "Missing Payload")))
        }
      }
  }
}

