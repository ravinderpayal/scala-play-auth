package com.rapidfork.auth.application.resetlog

import play.api.libs.json.{Json, OFormat}

case class VerifyResetTokenDto (
    email: String,
    token: String
  )
object VerifyResetTokenDto {
  implicit val format: OFormat[VerifyResetTokenDto] = Json.format[VerifyResetTokenDto]
}

case class ChangePasswordDto (
                                  email: String,
                                  token: String,
                                  password: String
                               )
object ChangePasswordDto {
  implicit val format: OFormat[ChangePasswordDto] = Json.format[ChangePasswordDto]
}

object ResetLogCustomDto {

}

