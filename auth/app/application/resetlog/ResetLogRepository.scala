package com.rapidfork.auth.application.resetlog

import javax.inject._
import play.api.db.slick.DatabaseConfigProvider

import scala.concurrent.{ExecutionContext, Future}
import slick.jdbc.MySQLProfile.api._
import com.rapidfork.auth.generated.resetlog
import com.rapidfork.auth.generated.resetlog.ResetLogSchema

@Singleton
class ResetLogRepository @Inject() (dbConfigProvider: DatabaseConfigProvider)(override implicit val ec: ExecutionContext) extends resetlog.ResetLogRepository(dbConfigProvider) {
  def getByEmail(email: String): Future[Seq[ResetLogSchema.ResetLog]] = {
    dbConfig.db.run(resetlogs.filter(_.email === email).result)
  }

  def updateMultiExpire(email: String): Future[Int] = {
    val rlq = resetlogs.filter(rl => (rl.email === email) && (rl.isExpired === false)).map(rl => (rl.isExpired)).update(true)
    dbConfig.db.run(rlq)
  }

  def getByTokenAndEmail(email: String, token: String): Future[Option[ResetLogSchema.ResetLog]] = {
    dbConfig.db.run(resetlogs.filter(rl => (rl.email === email) && (rl.token === token) && (rl.isExpired === false)).result.headOption)
  }

  def updatePassword(email: String, token: String): Future[Int] = {
    dbConfig.db.run(resetlogs.filter(rl => (rl.email === email) && (rl.token === token)).map(rl => rl.isReset).update(true))
  }
}

