package com.rapidfork.auth.application.user

import play.api.libs.json.{Json, OFormat}

case class UserLoginDto(
                            email: String,
                            password: String
                          )
object UserLoginDto {
  implicit val format: OFormat[UserLoginDto] = Json.format[UserLoginDto]
}

case class FilterUserDto (
                        firstName:  Option[String],
                        lastName:  Option[String],
                        mobile:  Option[Long],
                        email:  Option[String] ,
                        password:  Option[String],
                      )

object FilterUserDto {
  implicit val format: OFormat[FilterUserDto] = Json.format[FilterUserDto]
}

object UserCustomDto {

}

