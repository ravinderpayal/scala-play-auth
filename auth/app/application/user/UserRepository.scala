package com.rapidfork.auth.application.user

import javax.inject._
import play.api.db.slick.DatabaseConfigProvider

import scala.concurrent.{ExecutionContext, Future}
import slick.jdbc.MySQLProfile.api._
import com.rapidfork.auth.generated.user
import com.rapidfork.auth.generated.user.UserSchema

@Singleton
class UserRepository @Inject() (dbConfigProvider: DatabaseConfigProvider)(override implicit val ec: ExecutionContext) extends user.UserRepository(dbConfigProvider) {
  def getByEmail(email: String): Future[Option[UserSchema.User]] = {
    dbConfig.db.run(users.filter(_.email === email).result.headOption)
  }

  def updatePassword(email: String, password: String): Future[Int] = {
    dbConfig.db.run(users.filter(_.email === email).map(_.password).update(password))
  }
}

