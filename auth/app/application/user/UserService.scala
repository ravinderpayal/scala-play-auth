package com.rapidfork.auth.application.user

import javax.inject._

import scala.concurrent.{Await, ExecutionContext, Future}
import com.rapidfork.auth.generated.user
import com.rapidfork.auth.generated.user.{AddUserDto, UserPublic, UserSchema}
import com.rapidfork.auth.utils.{JwtData, JwtUtil}
import org.mindrot.jbcrypt.BCrypt
import pdi.jwt.{JwtBase64, JwtClaim, JwtJson}
import pdi.jwt.JwtSession.RichResult
import play.api.Configuration
import play.api.libs.json.Json
import play.api.mvc.{Result, Results}

import scala.concurrent.duration.Duration
import scala.util.{Failure, Success, Try}

@Singleton
class UserService @Inject()(userRepository: UserRepository)(implicit ec: ExecutionContext, configuration: Configuration) extends user.UserService(userRepository) {

  def getUserByEmail(email: String): Future[Option[UserSchema.User]] = userRepository.getByEmail(email.toLowerCase)

  def registerUser(addUserDto: AddUserDto): Future[String] = {
    val hashedPassword = BCrypt.hashpw(addUserDto.password, BCrypt.gensalt(12))
    var user = addUserDto.copy(password = hashedPassword)
    println(user)
    userRepository.add(user)
  }

  def validateUser(userLoginDto: UserLoginDto): Future[Result] = {

    userRepository.getByEmail(userLoginDto.email).map(
      user => {
        if (user.isDefined) {
          val userPublic = user.get
          val passwordMatched = BCrypt.checkpw(userLoginDto.password, userPublic.password)
          if (passwordMatched) {
            val token = JwtUtil.generate(userId = user.get.id, expireIn = (6*60*60).toLong)
            Results.Ok(Json.toJson(Json.obj("success" -> true, "status" -> 200, "accessToken" -> token)))
          } else {
            Results.Forbidden(Json.toJson(Json.obj("success" -> true, "status" -> 401, "message" -> "Invalid Email or Password")))
          }
        } else {
          Results.NotFound(Json.toJson(Json.obj("success" -> false, "status" -> 404, "message" -> "User Not Found")))
        }
      }
    )
  }

  def deSerialize(token: String): JwtData = {
    JwtUtil.deSerialize(token)
  }

  def validateJwt(token: String): Future[Try[UserPublic]] = {

    JwtUtil.validate(token) match {
      case Success(isValid) =>
        if (isValid) {
          val data = JwtUtil.deSerialize(token)
          userRepository.get(data.userId).map(u => Success(u.get.toPublic))
        } else {
          Future { Failure(new Exception("Auth Token is Invalid or Expired")) }
        }
      case Failure(t) => Future { Failure(new Exception("Auth Token is Invalid or Expired")) }
    }

  }

  def updatePassword(email: String, password: String): Future[Result] = {
    userRepository.getByEmail(email).flatMap(
      user => {
        if (user.isDefined) {
          val hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt(12))
          userRepository.updatePassword(email, hashedPassword).flatMap(
            up => Future { Results.Ok(Json.toJson(Json.obj("success" -> true, "status" -> 200, "message" -> "Password Changed Successfully"))) }
          )
        } else {
          Future { Results.NotFound(Json.toJson(Json.obj("success" -> false, "status" -> 404, "message" -> "User Not Found"))) }
        }
      }
    )
  }

}


