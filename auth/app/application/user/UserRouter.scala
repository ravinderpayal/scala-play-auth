package com.rapidfork.auth.application.user

import javax.inject._
import play.api.routing.Router.Routes
import play.api.routing.sird._

import com.rapidfork.auth.generated.user

class UserRouter @Inject()(controller: UserController ) extends user.UserRouter(controller) {
  override def routes: Routes = super.routes orElse  {
    case GET(p"/example") => controller.example
    case POST(p"/register") => controller.register
    case POST(p"/login") => controller.login
    case GET(p"/me") => controller.getProfile
    }
}


