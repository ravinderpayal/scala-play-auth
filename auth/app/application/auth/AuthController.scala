package com.rapidfork.auth.application.auth

import javax.inject._
import play.api.mvc._
import play.api.libs.json.{JsError, JsSuccess, Json}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}


@Singleton
class AuthController @Inject()(val controllerComponents: ControllerComponents, authService: AuthService)(implicit val ec: ExecutionContext) extends BaseController {

  def index: Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    Future { Ok("auth") }
  }

}
